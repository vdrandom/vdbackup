#!/usr/bin/env bash
# You can use config file instead of this script to set variables:
default_cfg=/usr/local/etc/backup.cfg
# Store backups, tar snapshots and exclusions here:
# local stuff:
bakdir_local=
host_local=${HOSTNAME} # in case I want to set a specific hostname
snapdir=/opt/backup/
exclfile=/opt/backup/exclusions.list
# remote stuff:
protocol='' #possible: ssh, ftp, sftp, local
use_gnupg=
bakdir_remote=
host_remote=
user_remote=
pass_remote= #for protocol='ftp' or sftp
port_remote= #leave empty for default
gnupg_key=

# Date/Time postfix:
postfix=$(date +%F-%H%M)

# MySQL backup user and pass in case you use them:
#db_user=backup
#db_pass=db17BakHz

# If you don't want to use config files, you can use these vars:
directories_dumb=''
directories_incr=''
databases=''
#incr_count=7

# If you want you can source variables above from config:
#if [[ -z ${using_ext_config} && -r ${default_cfg} ]]; then
#    source "${default_cfg}"
#else
#    echo "Default config not available and no options provided, using vars from script."
#fi

fix_port_numbers ()
{
    #fix ports in case they are not present in config
    if [[ -z ${port_remote} ]]; then
        [[ ${protocol} == 'ftp' ]] && port_remote=21
        [[ ${protocol} == 'sftp' || ${protocol} == 'ssh' ]] && port_remote=22
    fi
}

read_config ()
{
    # reset vars, especially in case they are also provided by this script
    directories_dumb=''
    directories_incr=''
    databases=''

    # check if we are using a valid config file and then run 'for' cycles with the config
    config="${OPTARG}"

    if [[ -r "${config}" ]]; then
        source "${config}"
        fix_port_numbers
    else
        echo "${config}: File inaccessable. You should use a readable file as an argument." >&2
        exit 1
    fi

    # run using config
    if [[ $# -eq 0 ]]; then
        using_config=1
        if [[ ${use_local} ]]; then
            [[ -d ${bakdir_local} ]] || echo "Missing local backup directory in config!" >&2 && exit 1
        fi
        # check if at least one variable is provided
        if [[ ${directories_dumb} || ${directories_incr} || ${databases} ]]; then
            if [[ ${directories_dumb} ]]; then
                for i in ${directories_dumb}; do
                    backup_dir_dumb
                done
            fi
            if [[ ${directories_incr} ]]; then
                for i in ${directories_incr}; do
                    backup_dir_incr
                done
            fi
            if [[ ${databases} ]]; then
                for i in ${databases}; do
                    backup_db
                done
            fi
        else
            echo "Nothing to backup, check your script configuration!" >&2
            exit 1
        fi
    fi

}

generate_name ()
{
    # check if we are using localdir to store backup
    if [[ ${protocol} == 'local' ]]; then
        [[ -d ${bakdir_local} ]] \
            || { echo "${bakdir} does not exist. You should specify an existing directory to store backups!" >&2; exit 1; }
        bakdir=${bakdir_local}
    else
        bakdir=${bakdir_remote}
    fi

    # check if we are creating sql backup
    if [[ ${baktype} == 'sql' ]]; then
        [[ -n "$(mysqlshow |grep ${src})" ]] \
            || { echo "${src}: There is no such database. You should use an existing database as an argument." >&2; exit 1; }
        outfile="${bakdir}/${host_local}-${src}_${postfix}.sql.gz"
    else
        [[ -d "${src}" ]] \
            || { echo "${src}: There is no such directory. You should use an existing directory for source directory as an argument." >&2; exit 1; }
        bakname="$(echo "${src}"|cut -c 2-|sed 's/\//_/g')"

        # separate basename from dirname
        srcpath="$(dirname ${src})"
        srcname="$(basename ${src})"

        if [[ ${baktype} != 'dumb' ]]; then
            [[ -d "${snapdir}" ]] \
                || { echo "${src}: There is no such directory. You should use an existing directory for snapshot directory as an argument." >&2; exit 1; }
            snapfile="${snapdir}/${bakname}.snar"
            [[ -r ${snapfile} ]] && baktype="incr" || baktype="full"
        fi
        outfile="${bakdir}/${host_local}-${bakname}_${postfix}_${baktype}.tar.gz"
    fi

    # check if we are using gnupg and send information about backup to stdout
    if [[ ${use_gnupg} ]]; then
        outfile="${outfile}.gpg"
    fi
}

info_to_stdout ()
{
    # peezdets, but should work
    if [[ ${protocol} == 'local' ]]; then
        stdout_proto='localhost'
    elif [[ ${protocol} == 'ssh' ]]; then
        stdout_proto="${host_remote} using ssh"
    elif [[ ${protocol} == 'ftp' ]]; then
        stdout_proto="${host_remote} using ftp"
    fi

    if [[ ${use_gnupg} ]]; then
        stdout_gpg=" encrypted"
    else
        stdout_gpg=''
    fi
    echo "Backing up ${src} (type: ${baktype})${stdout_gpg} to ${outfile} on ${stdout_proto}..."
}

store ()
{
    # locally
    if [[ ${protocol} == 'local' ]]; then
        dd of="${outfile}"
    # ssh
    elif [[ ${protocol} == 'ssh' ]]; then
        ssh -p${port_remote} ${user_remote}@${host_remote} "dd of=${outfile}"
    # (s)ftp
    elif [[ ${protocol} == 'ftp' || ${protocol} == 'sftp' ]]; then
        curl -ksS -T - ${protocol}://${host_remote}:${port_remote}/${outfile} -u ${user_remote}:${pass_remote}
    else
        echo "Wrong protocol!" >&2 && exit 1
    fi
}

# a workaround to count increments and remove snapshots; useless with remote storage
#backup_clean ()
#{
#    # count increments
#    test_ago=$(ls ${bakdir}/${bakname}*|tail -${incr_count}|wc -l)
#    if [[ ${test_ago} -lt ${incr_count} ]]; then
#        echo "Not enough incriment levels, skipping snapshot removal"
#    else
#        x_ago=$(ls ${bakdir}/${bakname}*|tail -${incr_count}|head -1|grep -oE 'full|incr')
#        if [[ "${x_ago}" == "full" ]]; then
#            rm "${snapfile}"
#        fi
#    fi
#}

backup_dir_dumb ()
{
    # check if we are using config or running explicitly to back up one directory
    if [[ ${using_config} ]]; then
        src="${i}"
    else
        src="${OPTARG}"
    fi

    # test directory and generate name
    baktype='dumb'
    generate_name

    # do it
    info_to_stdout
    if [[ ${use_gnupg} ]]; then
        tar czf - -C "${srcpath}" "${srcname}" --ignore-failed-read | gpg -r ${gnupg_key} -e - | store \
            && echo "Backup successfull." || echo "Backup failed."
    else
        tar czf - -C "${srcpath}" "${srcname}" --ignore-failed-read | store \
            && echo "Backup successfull." || echo "Backup failed."
    fi
}

backup_dir_incr ()
{
    # check if we are using config or running explicitly to back up one directory
    if [[ ${using_config} ]]; then
        src="${i}"
    else
        src="${OPTARG}"
    fi

    # test directory and generate name
    #backup_clean - use it here or don't use at all!
    baktype='incr'
    generate_name

    # do it
    info_to_stdout

    [[ -f ${exclfile} ]] || touch ${exclfile} # create empty exclusions file so that tar doesn't complain about it, peezdets, but should work.

    if [[ ${use_gnupg} ]]; then
        tar czf - -C "${srcpath}" "${srcname}" -g "${snapfile}" -X "${exclfile}" --ignore-failed-read | gpg -r ${gnupg_key} -e - | store \
            && echo "Backup successfull." || echo "Backup failed."
    else
        tar czf - -C "${srcpath}" "${srcname}" -g "${snapfile}" -X "${exclfile}" --ignore-failed-read | store \
            && echo "Backup successfull." || echo "Backup failed."
    fi
}

backup_db ()
{
    # check if we are using config or running explicitly to back up one database
    if [[ ${using_config} ]]; then
        src="${i}"
    else
        src="${OPTARG}"
    fi

    baktype=sql
    generate_name

    # do it
    info_to_stdout
    if [[ ${use_gnupg} ]]; then
        mysqldump --complete-insert --no-create-db ${src} | gzip | gpg -r ${gnupg_key} -e - | store \
            && echo "Backup successfull." || echo "Backup failed."
    else
        mysqldump --complete-insert --no-create-db ${src} | gzip | store \
            && echo "Backup successfull." || echo "Backup failed."
    fi
}

# simple function to encrypt and upload backup; don't want to use it here at all
#upload_backup ()
#{
#    gpg -r ${key} -e ${filename}|ssh -p${port} ${user}@${server} "dd of=${path}/${filename}"
#}

help_section ()
{
    echo "Usage: backup.sh [-h] [-d /path] [-i /path] [-b database_name] [-f /path]
    if you run it without any options, it will use settings from script or fail trying
    -h show this text
    -f read config for stuff to backup
    -b create backup of a specific database
    -d create dumb backup of a specific directory
    -i create incremental backup of a specific directory"
}


while getopts ":d:b:i:f:ch" Option; do
    case ${Option} in
    h ) help_section;;
    f ) read_config;;
    b ) backup_db;;
    d ) backup_dir_dumb;;
    i ) backup_dir_incr;;
    esac
done

if [[ ${default_cfg} && -z ${config} ]]; then
    OPTARG="${default_cfg}"
    read_config
elif [[ -z ${config} ]]; then
    echo "No configuration file or options provided. Exiting." >&2
    exit 1
fi

exit 0
